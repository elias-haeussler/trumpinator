[![build](https://travis-ci.com/eliashaeussler/trumpinator.svg)](https://travis-ci.com/eliashaeussler/trumpinator)
[![license](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE.md)
[![release](https://img.shields.io/badge/release-v1.0.0-blue.svg)](https://bitbucket.org/elias-haeussler/trumpinator/downloads)

# Trumpinator
Welcome to Trumpinator, the world's best opportunity to deal with Donald Trump's statements! Don't you like what he's talking about on Twitter? No problem, just scream everything you have on your mind inside your computer's mic and it will destroy all the crappy stuff [@realDonaldTrump](https://twitter.com/realDonaldTrump) is talking about on Twitter!

## What the hell is that?
*Trumpinator* is our new method to help you get rid of your bad mood in relation to Donald Trump. We all know that he's a unsuitable president and we want him to know our frustration about him. So we decided to develop a tiny program which reads the lastest 200 tweets of [@realDonaldTrump](https://twitter.com/realDonaldTrump) and enables you to destroy all of them by screaming inside your computer's mic. You will see, it's freeing! :-)

## How to start
You can either run the program using the Processing project or the exported Build files.

### Processing source code
1. Clone or download the project from GitHub
2. Open Processing
3. Open the file *trumpinator.pde*. **Important: All files and folders need to be inside a folder called *trumpinator* (which they are, by default, when you clone the repository)**
4. Press *Run* (or Cmd+R / Ctrl+R)

### Build files
Download the Build files from the [Downloads](https://bitbucket.org/elias-haeussler/trumpinator/downloads) section for your Operating System and run them. The following systems are currently supported:

* macOS
* Windows (32-/64-bit)
* Linux (32-/64-bit)

## Requirements
There is just one thing you need: Download and install at least **Java 8** to run the program. You can get the latest version of Java directly here: https://www.java.com/download/

## Resources we have used
* Twitter4J (https://github.com/yusuke/twitter4j)
* Minim (https://github.com/ddf/Minim/)
* Some fonts downloaded from http://www.dafont.com/

## License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
